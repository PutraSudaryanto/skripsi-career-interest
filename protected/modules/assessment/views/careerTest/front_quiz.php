<?php
/**
 * Assessment Career Tests (assessment-career-tests)
 * @var $this TestController
 * @var $model AssessmentCareerTests
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 29 July 2018, 18:24 WIB
 * @modified date 29 July 2018, 18:24 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 */

	$this->breadcrumbs=array(
		'Assessments'=>Yii::app()->controller->createUrl('member/index'),
		'Career'=>Yii::app()->controller->createUrl('index'),
	);

	$message_instruction = $this->parseTemplate($setting->message_alert['instruction'], array(
		'period_test_difference'=>$setting->period_test_difference,
		'period_test_diff_type'=>AssessmentCareerSetting::getPeriodTestDiffType($setting->period_test_diff_type),
	));
	$message_instruction = preg_replace('/\s\s+/', '', $message_instruction);
	$test_url = Yii::app()->controller->createUrl('quiz');
	$index_url = Yii::app()->createUrl('site/index');

	$testCondition = $testCondition == true ? 1 : 0;
	$stepMax = $statementGroup->group_total_i;
	$stepFrom = ($stepMax - $statementGroupCount) + 1;

	$cs = Yii::app()->getClientScript();
	$cs->registerCssFile(Yii::app()->theme->baseUrl.'/plugins/sweetalert/sweetalert.css');
	$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/plugins/sweetalert/sweetalert.min.js', CClientScript::POS_END);
	$cs->registerCssFile(Yii::app()->theme->baseUrl.'/plugins/ion-rangeslider/css/ion.rangeSlider.css');
	$cs->registerCssFile(Yii::app()->theme->baseUrl.'/plugins/ion-rangeslider/css/ion.rangeSlider.skinFlat.css');
	$cs->registerScriptFile(Yii::app()->theme->baseUrl.'/plugins/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.js', CClientScript::POS_END);
$js=<<<EOP
	var testCondition = $testCondition;
	var testUrl = location.href;
	var form = $('#assessment-career-test');
	var formAction = form.attr('action');

	var stepMax = $stepMax;
	var stepFrom = $stepFrom;

	$(function () {
		$('.js-sweetalert button').on('click', function() {
			var type = $(this).data('type');
			if (type === 'to-instruction') {
				showAlertMessage();
			}
		});
	});
	function showAlertMessage() {
		swal({
			title: 'Instruction',
			text: '$message_instruction',
			showCancelButton: true,
			confirmButtonText: 'Lanjut',
			closeOnConfirm: false,
			html: true
		}, function () {
			location.href = '$test_url';
		});
	}
	if(testCondition == 0) {
		runTest();
		$('#defaultModal').on('show.bs.modal', function (event) {
			var modal = $(this);
			modal.find('.modal-title').text('Instruksi');
			modal.find('.modal-body').html('$message_instruction');
			modal.find('#modalSubmit').text('Mulai');
			modal.find('#modalCancel').text('Batal');
		});
		$('#defaultModal').modal('show');
	
		$('#modalSubmit').on('click', function() {
			$.ajax({
				method: 'POST',
				url: testUrl,
				data: {start: 'true'},
				success: function(response) {
					location.reload();
				},
				error: function(jqXHR, textStatus, error) {
					location.href = testUrl;
				}
			});
		});
	
		$('#modalCancel').on('click', function() {
			location.href = '$index_url';
		});
	} else {
		runTest();
		form.find('input').on('change', function() {
			$.ajax({
				method: 'POST',
				url: testUrl,
				data: form.serialize(),
				dataType: 'json',
				success: function(response) {
					var hasError = 0;
					if(countProperties(response) > 0) {
						for(i in response) {
							if(i.indexOf('_'))
								hasError = 1;
						}
						if(hasError == 1) {
							$('form[action="'+formAction+'"]').find('div.errorMessage').hide().html('');
							for(i in response) {
								$('form[action="'+formAction+'"] #' + i + '_em_').show().html(response[i][0]);
							}
						}
					} else
						location.reload();
				},
				error: function(jqXHR, textStatus, error) {
					location.href = testUrl;
				}
			});
		});
	}
	
	function countProperties(obj) {
		var prop;
		var propCount = 0;
	
		for (prop in obj) {
			propCount++;
		}
		return propCount;
	}
	
	function runTest() {
		$('#range_01').ionRangeSlider({
			min: 0,
			max: stepMax,
			from: stepFrom
		});
	}
EOP;
	$cs->registerScript('dialog-alert', $js, CClientScript::POS_END);
?>

<div class="card">
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="header">
				<input id="range_01"/>
			</div>
		</div>
	</div>
</div>

<?php
$statements = $statementGroup->statements;
$statement_choice = AssessmentCareerTestDetail::getStatementChoice();
if(!$testCondition || !empty($statements)) {
	$model=new AssessmentCareerTestDetail;

	$form=$this->beginWidget('application.libraries.yii-traits.system.OActiveForm', array(
		'id'=>'assessment-career-test',
		'enableAjaxValidation'=>true,
		/*
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
			'on_post' => '',
		),
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
		*/
	));?>
<div class="card question">
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="header">
				<div class="progress">
					<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> <span class="sr-only">60% Complete</span> </div>
				</div>
			</div>
		</div>
	</div>
	<?php /*
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="body">
				<div class="row">
				<?php foreach($statement_choice as $key => $val) {?>
					<div class="col-lg-2 col-md-3 col-sm-3 align-center">
						<?php echo preg_replace('/ /', "<br/>", $val).'<br/>';?>
					</div>
				<?php }?>
				</div>
			</div>
		</div>
	</div>
	*/?>
	<?php 
	$statements = (array)$statements;
	shuffle($statements);
	foreach($statements as $key => $val) {?>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="body">
				<h5><?php echo $val->statement->message;?></h5>
				<?php echo $form->error($model, 'statement_id_i['.$val->statement_id.']'); ?>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12">
			<div class="body">
				<?php echo $form->radioButtonList($model, 'statement_id_i['.$val->statement_id.']', $statement_choice);?>
			</div>
		</div>
	</div>
	<?php }?>
</div>
<?php $this->endWidget();
} else {?>
	aaaaa
<?php }?>