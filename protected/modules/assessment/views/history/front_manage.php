<?php
/**
 * Assessment Career Tests (assessment-career-tests)
 * @var $this HistoryController
 * @var $model AssessmentCareerTests
 *
 * @author Putra Sudaryanto <putra@sudaryanto.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 Ommu Platform (www.ommu.co)
 * @created date 29 July 2018, 18:24 WIB
 * @modified date 29 July 2018, 18:24 WIB
 * @link https://bitbucket.org/ommu/ps-career-interest
 *
 */

	$this->breadcrumbs=array(
		'Assessments'=>Yii::app()->controller->createUrl('member/index'),
	);
?>

<div id="partial-assessment-career-tests">
	<?php //begin.Messages ?>
	<div id="ajax-message">
	<?php
	if(Yii::app()->user->hasFlash('error'))
		echo $this->flashMessage(Yii::app()->user->getFlash('error'), 'error');
	if(Yii::app()->user->hasFlash('success'))
		echo $this->flashMessage(Yii::app()->user->getFlash('success'), 'success');
	?>
	</div>
	<?php //begin.Messages ?>

	<div class="boxed">
		<?php //begin.Grid Item ?>
		<?php 
			$columnData   = array(
				array(
					'header' => Yii::t('app', 'No'),
					'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
					'htmlOptions' => array(
						'class' => 'center',
					),
				), array(
					'name' => 'start_date',
					'value' => '!in_array($data->start_date, array(\'0000-00-00 00:00:00\', \'1970-01-01 00:00:00\', \'0002-12-02 07:07:12\', \'-0001-11-30 00:00:00\')) ? Yii::app()->dateFormatter->formatDateTime($data->start_date, \'medium\', \'long\') : \'-\'',
					'htmlOptions' => array(
						'class' => 'center',
					),
					'filter' => $this->filterDatepicker($model, 'start_date'),
				), array(
					'name' => 'next_test_date',
					'value' => '!in_array($data->next_test_date, array(\'0000-00-00 00:00:00\', \'1970-01-01 00:00:00\', \'0002-12-02 07:07:12\', \'-0001-11-30 00:00:00\')) ? Yii::app()->dateFormatter->formatDateTime($data->next_test_date, \'medium\', \'long\') : \'-\'',
					'htmlOptions' => array(
						'class' => 'center',
					),
					'filter' => $this->filterDatepicker($model, 'next_test_date'),
				), array(
					'header' => Yii::t('phrase', 'Status'),
					'name' => 'publish',
					'value' => '$data->publish == 1 ? ($data->interest_i == 0 ? CHtml::tag(\'span\', array(\'class\'=>\'label label-warning\'), \'Pending\') : ($data->interest_i == 1 ? CHtml::tag(\'span\', array(\'class\'=>\'label label-warning\'), \'Tidak Memiliki Minat\') : ($data->interest_i == 3 ? CHtml::tag(\'span\', array(\'class\'=>\'label label-success\'), \'Memiliki Minat Spesifik\') : CHtml::tag(\'span\', array(\'class\'=>\'label label-info\'), \'Belum Memiliki Minat Spesifik\')))) : CHtml::tag(\'span\', array(\'class\'=>\'label label-danger\'), \'Cancel\')',
					'htmlOptions' => array(
						'class' => 'center',
					),
					'type' => 'raw',
				),
			);
			array_push($columnData, array(
				'header' => Yii::t('phrase', 'Options'),
				'class' => 'CButtonColumn',
				'buttons' => array(
					'view' => array(
						'label' => Yii::t('phrase', 'Result'),
						'imageUrl' => Yii::app()->params['grid-view']['buttonImageUrl'],
						'options' => array(
							'class' => 'view',
							'title' => Yii::t('phrase', 'Assessment Career Test Result'),
						),
						'url' => 'Yii::app()->controller->createUrl(\'careerTest/result\', array(\'id\'=>$data->primaryKey))'),
				),
				'template' => '{view}',
			));

			$this->widget('application.libraries.yii-traits.system.OGridView', array(
				'id'=>'assessment-career-tests-grid',
				'dataProvider'=>$model->search(),
				//'filter'=>$model,
				'columns'=>$columnData,
				'template'=>'{items}<div class="row"><div class="col-sm-12 col-md-12 col-lg-4 p-b-20">{summary}</div><div class="col-sm-12 col-md-12 col-lg-8 align-right">{pager}</div></div>',
				'pager'=>array(
					'header'=>'',
					'htmlOptions'=>array(
						'class'=>'pagination',
					),	
					'internalPageCssClass'=>'page-item',
					'cssFile' => '',
				),
				'afterAjaxUpdate'=>'reinstallDatePicker',
				'cssFile' => '',
				'pagerCssClass'=>'d-lg-inline-block',
			));
		?>
		<?php //end.Grid Item ?>
	</div>
</div>